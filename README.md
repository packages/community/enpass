# enpass

A multiplatform password manager

To check if there's a new version available:

```
curl -so- https://apt.enpass.io/dists/stable/main/binary-amd64/Packages.gz | zcat | egrep "Package|Version|SHA256" | head -n 3
```
